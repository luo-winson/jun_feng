---
layout: category
title: 网新小白的疑惑：网站设计是什么？
categories:
    - 网站设计
---
初入网络与新媒体领域，对一切都是未知和迷惘，网站设计究竟是什么？

<!--more-->

## 网站设计是什么

通过简答的搜索寻找得知，网站设计的**目的就是产生网站**。
网站设计是将策划案中的内容、网站的主题模式，以及结合自己的认识**通过艺术的手法表现出来**。
网站设计要能**充分吸引访问者的注意力**，让访问者**产生视觉上的愉悦感**。

## 网站设计的内容

 - logo

网站LOGO是该网站为了体现其特色与内涵，设计制作了一个LOGO图像放在网站左上角或其他醒目的位置。
公司网站常常使用企业标志或注册商标。
如若是个人网站LOGO应该以个人清晰照为主。

 - 导航条

导航条是网站页面的重要组成元素。

其设计的目的是将网站里面的信息进行分类，帮助浏览者快速找到他需要的信息。

**作用**

1.引导用户完成网站各内容页面间的跳转。

2.理清网站各内容与链接之间的关系，例如展现整个网站的目录信息，帮助用户快速找到内容。

3.对于用户在网站中所处的位置，尤其是面包屑导航。


导航条的样式有很多种，最常见的有以下几种：

1.水平导航栏（包括顶部导航栏、底部导航栏）

2.侧边导航栏（包括左侧导航栏、右侧导航栏）

3.文字导航栏（例如运用文字将内容分类）

4.图表导航栏（例如运用相关图像进行指示）

 - Banner

译文为横幅，即是网络页面中的最为醒目的图片，通常用作内容页面里的广告部分
Banner放置的位置通常在醒目的位置，如logo相邻处，以吸引浏览者浏览。

 - 内容版块

内容板块指的是整个网站页面里面重要的组成部分。
网站设计者可以通过该网站页面信息分类来设计不同内容的板块，犹如杂志的分类内容。
且不同的板块有着不同的标题和不同的文本信息。
例如[知乎](https://www.zhihu.com/)

 - 版尾

版尾是页面最低端的版块，用于表明网页注释内容或作者信息等等
通常放置网站所有者、设计者的联系方式，版权结构所有，网页内信息的出处或是制作该网页的年份和网站的名字。

